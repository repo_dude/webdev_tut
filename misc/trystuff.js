// Creating objects

const obj1 = {
	name: 'John',
	surname: 'Doe'
}

console.log(obj1)

// Can also have methods/functions 

const obj2 = {
	name:'John',
	surname: 'Doe',
	printName: function(){
		return `${this.name} ${this.surname}`
	}
}

console.log(obj2)
console.log(obj2.printName())

// ======================== constructors =======================
// Parameterised way of creating objects

function person(name, surname){
	this.name = name,
	this.surname = surname
}

const p = new person('John', 'Malkovich');
console.log(p);

// with methods 

function person2(name, surname){
	this.name = name,
	this.surname = surname
	this.printName = function(){
		return `${this.name} ${this.surname}`
	}
}

const p2 = new person2('Paul', 'Dirac');
console.log(p2);

// efficient way for methods is to use Object.prototype

function person3(name, surname){
	this.name = name,
	this.surname = surname

}

person3.prototype.printName = function(){
		return `${this.name} ${this.surname}`
}


const p3 = new person3('Paul', 'Dirac');
console.log(p3, p3.printName());

// using .prototype is useful when we have already existing objects and we want 
// to add properties or methods to all of them.


// Modern ES6 class keyword. This is syntactic sugar of the above, all wrapped up
// in nice class like statements.


// private properties and methodes start with #
// if name was private it would be this.#name = name
class person4 {

    constructor(name, surname){
		this.name = name,
		this.surname = surname
    }

    printName(){
		return `${this.name} ${this.surname}`
	}
}

const p4 = new person4('Paul', 'Dirac');
console.log(p4, p4.printName());


















